cmake_minimum_required(VERSION 3.25)
project(ipi VERSION 0.3.1.0 DESCRIPTION "Integer Partition Iterator" LANGUAGES CXX)
enable_testing()

include(${PROJECT_SOURCE_DIR}/cmake/conan.cmake)
conan_cmake_configure(REQUIRES boost/1.81.0 GENERATORS CMakeDeps CMakeToolchain)
conan_cmake_autodetect(settings)
conan_cmake_install(PATH_OR_REFERENCE . BUILD missing REMOTE conancenter SETTINGS ${settings})

include(${CMAKE_BINARY_DIR}/conan_toolchain.cmake)
find_package(Boost 1.81.0 EXACT REQUIRED COMPONENTS program_options)

add_subdirectory(src)
add_subdirectory(test)