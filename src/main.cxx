#include <cstdlib>

#include <iostream>
#include <ranges>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <boost/program_options.hpp>

#include "partitions.hxx"
#include "version.hxx"

auto parse(int argc, char *argv[]) -> std::tuple<boost::program_options::options_description, boost::program_options::variables_map>
{
    boost::program_options::options_description options("Options");
    options.add_options()
        ("help,h", "show this message")
        ("version,v", "program version")
        ("count,c", "count")
        ("ferrers,f", "Ferrers diagrams")
        ("sep,s", boost::program_options::value<std::string>()->default_value(" ", "<space>"), "part delimiter")
        ("br,b", boost::program_options::value<std::string>()->default_value("\n", "<newline>"), "partition delimiter");

    boost::program_options::options_description hidden("Hidden");
    hidden.add_options()
        ("number,n", boost::program_options::value<std::size_t>(), "number to partition");

    boost::program_options::positional_options_description positional;
    positional.add("number", 1);

    boost::program_options::variables_map map;
    boost::program_options::command_line_parser parser(argc, argv);
    parser.options(hidden.add(options));
    parser.positional(positional);
    boost::program_options::store(parser.run(), map);
    boost::program_options::notify(map);

    return std::make_tuple(std::move(options), std::move(map));
}

auto is_valid(const boost::program_options::variables_map& map) -> bool
{
    static const std::vector<std::string> expected = {"number", "version"};
    return std::ranges::any_of(expected, [&](const auto& s){ return map.count(s); });
}

auto delimiters(const boost::program_options::variables_map& map) -> std::tuple<std::string_view, std::string_view>
{
    const std::string_view sep = map["sep"].as<std::string>();
    const std::string_view br = map["br"].as<std::string>();
    return std::make_tuple(std::move(sep), std::move(br));
}

enum struct format
{
    composition,
    ferrers
};

template <format F>
struct printable {
    std::size_t n;
    std::string_view sep;
    std::string_view br;
};

template <format F>
auto operator<<(std::ostream& os, const printable<F>& p) -> std::ostream& {
    const std::vector<char> dots(p.n, '*');
    for (std::string_view br = ""; const auto& a : ipi::partitions(p.n)) {
        os << br;
        br = p.br;
        for (std::string_view sep = ""; const auto& x : a) {
            if constexpr (F == format::composition) {
                os << sep << x;
            } else {
                os << std::string_view(dots.data(), x) << p.sep;
            }
            sep = p.sep;
        }
    }
    return os;
}

auto main(
    int argc,
    char * argv[]
)->int
{
    try
    {
        if (const auto& [options, map] = parse(argc, argv); map.count("help") or !is_valid(map))
        {
            std::cout << "Usage: " << argv[0] << " [option(s)] NUMBER\n";
            std::cout << "Prints the integer partitions of NUMBER as compositions.\n";
            std::cout << options;
        }
        else if (map.count("version"))
        {
            std::cout << ipi::version() << '\n';
        }
        else
        {
            if (const std::size_t n = map["number"].as<std::size_t>(); map.count("count"))
            {
                std::cout << std::ranges::distance(ipi::partitions(n)) << '\n';
            }
            else if (auto [sep, br] = delimiters(map); map.count("ferrers"))
            {
                sep = map["sep"].defaulted() ? "\n" : sep;
                br = map["br"].defaulted() ? "\n" : br;
                std::cout << printable<format::ferrers>{.n=n, .sep=sep, .br=br};
            }
            else
            {
                std::cout << printable<format::composition>{.n=n, .sep=sep, .br=br} << '\n';
            }
        }
    }
    catch(const std::exception& error)
    {
        std::cerr << "error: " << error.what() << '\n';
        return EXIT_FAILURE;
    }
    catch(...)
    {
        std::cerr << "error: unknown\n";
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}