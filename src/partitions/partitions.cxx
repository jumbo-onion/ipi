#include <vector>

#include "partitions.hxx"

extern auto ipi::partitions(std::size_t n) -> std::generator<std::span<std::size_t>>
{
    if (n == 0) {
        co_return;
    }
    std::vector<std::size_t> a(n, 0);
    std::size_t k = 2;
    std::size_t y = n - 1;
    while (k != 1) {
        k--;
        std::size_t x = a[k-1] + 1;
        while (2*x <= y) {
            a[k-1] = x;
            y = y - x;
            k++;
        }
        std::size_t l = k + 1;
        while (x <= y) {
            a[k-1] = x;
            a[l-1] = y;
            co_yield std::span(a.data(), l);
            x++;
            y--;
        }
        y = y + x - 1;
        a[k-1] = y + 1;
        co_yield std::span(a.data(), k);
    }
}