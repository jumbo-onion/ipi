#pragma once

#include <span>

#include "generator.hxx"

namespace ipi
{
    extern auto partitions(std::size_t n) -> std::generator<std::span<std::size_t>>;
}