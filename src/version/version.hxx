#pragma once

#include <string_view>

namespace ipi
{
    extern auto version() noexcept -> std::string_view;
}