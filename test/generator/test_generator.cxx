#include <cstdlib>

#include <iostream>

#include "generator.hxx"

auto countdown(unsigned long n)->std::generator<unsigned long>
{
    do {
        co_yield n;
    } while (n-- > 0);
}

auto main(int argc, char *argv[])->int
{
    try
    {
        if (argc != 2)
            throw std::invalid_argument("expected one input");

        for (auto n = std::stoul(argv[1]); auto i : countdown(n))
            std::cout << i << ' ';
        std::cout << '\n';
    }
    catch(const std::exception& error)
    {
        std::cerr << "error: " << error.what() << '\n';
        return EXIT_FAILURE;
    }
    catch(...)
    {
        std::cerr << "error: unknown\n";
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}