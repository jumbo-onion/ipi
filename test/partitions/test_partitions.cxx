#include <cstdlib>

#include <iostream>
#include <ranges>

#include "partitions.hxx"

auto main(int argc, char *argv[])->int
{
    try
    {
        if (argc != 2)
            throw std::invalid_argument("expected one input");

        const auto n = std::stoul(argv[1]);
        std::cout << std::ranges::distance(ipi::partitions(n)) << '\n';
    }
    catch(const std::exception& error)
    {
        std::cerr << "error: " << error.what() << '\n';
        return EXIT_FAILURE;
    }
    catch(...)
    {
        std::cerr << "error: unknown\n";
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}