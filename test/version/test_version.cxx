#include <cstdlib>

#include <iostream>

#include "version.hxx"

auto main(
    int argc,
    char * argv[]
)->int
{
    try
    {
        std::cout << ipi::version() << '\n';
    }
    catch(const std::exception& error)
    {
        std::cerr << "error: " << error.what() << '\n';
        return EXIT_FAILURE;
    }
    catch(...)
    {
        std::cerr << "error: unknown\n";
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}